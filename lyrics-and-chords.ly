\version "2.16.2"

\header {
  title = "L'orage"
  instrument = "Guitare et chant"
  composer = "Georges Brassens"
  tagline = "stepan@ithaca.fr"
}


<<
  
   \chords{
      \set chordChanges = ##t
       s2 f1:m f1:m f1:m f1:m bes1:7 bes1:7 bes1:7 bes1:7
       ees1 ees1 ees1 ees1 g1:7 g1:7 g1:7 g1:7
       c1:m c1:m d1:7 g1:7 c1:m aes2 g2:7 aes2 g2:7 c2
   }

  
  \relative c' {
    #(set-accidental-style 'default 'Voice)
    \key c \minor
    \time 4/4
    \repeat volta 8 {
    \partial 2 c4 c4 
      
    aes'4 aes4 aes4 aes4( aes2) f4 g4 aes4 f4 g4 aes4( aes2)
    bes,4 d4 f4 d4 f4 aes4( aes4) f4 g4 aes4 c2 bes2 aes1( aes4)
    bes,4 a4 bes4 aes'4 g4 fis4 g4 \times 2/3 {aes2(bes,2) g'2(} g2)
    g,4 g4 f'4 f4 f4 f4( f2) d4 ees4 f4 aes4  g4  f4( f2)
    g,4 g4 ees'4 g4 f4 ees4( ees2.) ees4 d4 a4 d4 c4 b1( b4)
    c4 ees4 g4 c4 bes4 g4 f4 ees2( d2) c2\fermata r2 } 
  }
  
  \addlyrics {
    Par -- lez moi de la pluie et non pas du beau temps,
    Le beau temps me dé -- goûte et m'fait grin -- cer les dents,
    Le bel a -- zur me met en ra -- ge,
    Car le plus grand a -- mour qui m'fut don -- né sur terr'
    Je l'dois au mau -- vais temps, je l'dois à Ju -- pi -- ter
    Il me tom -- ba d'un ciel d'o -- ra -- -- -- -- ge.
  }
  
  \addlyrics {
    Par un soir de no -- vembre, à che -- val sur les toits,
    Un vrai ton -- nerr' de Brest, a -- vec ces crits d'pu -- tois
    Al -- lu -- mait ces feux d'ar -- ti fi -- ce.
    Bon -- di -- ssant de sa couche en cos -- tu -- me de nuit,
    Ma voi -- sine af -- fo lé' vint co -- gner à mon huis
    En ré -- cla -- mant mes bons of -- fi -- ces.
  }
  
  \addlyrics {
    «Je suis seule et j'ai peur, ou -- vrez moi, par pi -- tié,
    Mon é -- poux vient d'par -- tir, fai -- re son dur mé -- tier,
    Pau -- vre mal -- heu -- reux mer -- ce -- nai -- re,
    Con -- traint d'cou -- cher de -- hors quand il fait mau -- vais temps,
    Pour la bon -- ne rai -- son qu'il est re -- pré -- sen -- tant
    D'un' mai -- son de pa -- ra -- ton -- ner -- res.»
  }
  
  \addlyrics {
    En bé -- ni -- ssant le nom de Ben -- ja -- min Fran -- klin,
    Je l'ai mise en lieu sûr, en -- tre mes bras câ -- lins,
    Et puis l'a -- mour a fait le res -- te!
    Toi qui sèmes des pa ra ton ner -- re' à foi -- son,
    Que n'en n'as tu plan -- té sur ta pro -- pre mai -- son?
    Er -- reur on ne peut plus fu -- ne -- ste. 
  }
  
  \addlyrics {
    Quand Ju -- pi -- ter al -- la  se faire en -- tendre ail -- leurs,
    La belle, ay -- ant en -- fin con -- ju -- ré sa fray -- eur
    Et re -- cou -- vré tout son cou -- ra -- ge,
    Ren -- tra dans ses foy -- ers fair' sé -- cher son ma -- ri
    En m'don -- nant ren -- dez- vous les jours d'in -- temp -- pé ri'
    Ren -- dez- vous au pro -- chain o -- ra -- ge.
  }	
  
  \addlyrics {
    À par -- tir de ce jour j'na'i plus bais -- sé les yeux,
    J'ai con -- sa -- cré mon temps à con -- tem -- pler les cieux,
    À re -- gar -- der pas -- ser les nu -- es,
    À guet -- ter les stra -- tus, à lor -- gner les nim -- bus,
    À fai -- re les yeux doux aux mon -- dres cu -- mu -- lus,
    Mais el -- le n'est pas re -- ve -- nu -- e.
  }
  
  \addlyrics {
    Son bon -- homm' de ma -- ri, a -- vait tant fait d'af -- fair's,
    Tant ven -- du ce soir- là de pe -- tits bouts de fer,
    Qu'il é -- tait de -- v'nu mil -- lion -- nai -- re
    Et l'a -- vait em -- me né' vers des cieux tou -- jours bleus,
    Des pa -- ys im -- bé -- cile' où ja -- mais il ne pleut,
    Où l'on ne sait rien du ton -- ner -- re.
  }
  
  \addlyrics {
    Dieu fass' que ma com plainte ail -- le, tam -- bour bat -- tant,
    Lui par -- ler de la plui', lui par -- ler du gros temps
    Aux -- quels on a t'nu tête en -- sem -- ble,
    Lui con -- ter qu'un cer -- tain coup de foudre as -- sa -- sin
    Dans le mill' de mon coeur a lais --  sé  le des -- sin
    D'un' pe -- tit' fleur qui lui res -- sem -- ble.
  }
  
  
>>